from reader.circuit.model.ElementTypeEnum import ElementTypeEnum


class Element(object):
    
    type: ElementTypeEnum
    name: str

    def __init__(self, type:ElementTypeEnum, name:str):
        self.type = type
        self.name = name