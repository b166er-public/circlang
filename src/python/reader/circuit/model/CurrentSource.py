from reader.circuit.model.Component import Component
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class CurrentSource(Component):

    current: float

    def __init__(self, name:str, pin_in_name:str, pin_out_name:str, current:float):
        super().__init__(ElementTypeEnum.CURRENT_SOURCE, name, pin_in_name, pin_out_name)
        self.current = current