from reader.circuit.model.Component import Component
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class Resistor(Component):
    
    resistance: float

    def __init__(self, name:str, pin_in_name:str, pin_out_name:str, resistance:float):
        super().__init__(ElementTypeEnum.RESISTOR, name, pin_in_name, pin_out_name)
        self.resistance = resistance