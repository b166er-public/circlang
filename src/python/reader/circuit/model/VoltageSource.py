from reader.circuit.model.Component import Component
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class VoltageSource(Component):

    voltage: float

    def __init__(self, name:str, pin_in_name:str, pin_out_name:str, voltage:float):
        super().__init__(ElementTypeEnum.VOLTAGE_SOURCE, name, pin_in_name, pin_out_name)
        self.voltage = voltage