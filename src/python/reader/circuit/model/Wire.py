from reader.circuit.model.Component import Component
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class Wire(Component):
    
    def __init__(self, name:str, pin_in_name:str, pin_out_name:str):
        super().__init__(ElementTypeEnum.WIRE, name, pin_in_name, pin_out_name)