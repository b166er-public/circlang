# Thoughts about symbolic

## Language concepts

## Knowledge base
The complete knowledge of the linear system is modelled within a structure called ***knowledge base***. The knowledge base holds a map of ***terms*** and a map of ***equations***.

## Phases
The process of solving a given linear system is seperated into two phases:
- ***analysis phase*** which transforms the initial knowledge base with all the information known about the linear system to be solved into a state, where sequence of ***evaluable terms*** can be generated from which a subset evaluates to the solution of the system
- ***calculation phase*** during which the early generated sequence of terms is actually evaluated and in the end the user receives the results of those terms which represent the components of the linear system solution

## Equation
An equation modells the equallity of two distinct ***terms*** within the knowledge base. The knowledge base allows manipulation of equations using ***terms*** within the knowledge base or by additive combination with other known equations.

## Term
Every ***numerical value*** in the language is modelled by ***terms***.

The ***atomar terms*** are entities which are not further expanded within the language. Following atomar terms are currently supported:
- Constant (c)
- Parameter (p)
- Variable (x)

The ***composed terms*** are entities which requires the process of ***evaluation*** to retrieve the corresponding numerical value. Following composed terms are currently supported:
- Sum (+)
- Product (*)
- Negation (-)
- Inversion (/)

## Evaluation tree
The process of ***evaluation*** is performed using a non-cyclic tree called ***evaluation tree***.

- Every atomar term has a trivial evaluation tree consisting of a single node referencing the term.
- Every composed term has a ***partial evaluation tree*** consisting of a root representing the current term and the directly linked ***child terms*** needed for evaluation of the composed term.
- The child terms of a composed term can be further expanded recursivly until all leafs become ***atomar terms*** - the tree is then called a ***full evaluation tree***


## Term evaluation
The process of mapping between the language construct of ***terms*** and their corresponding ***numerical values*** is called ***evaluation***. 

An evaluation requires the deduction of a ***full evaluation tree***. Additionally the full evaluation tree is not allowed to contain any variable term. Such a tree is called ***variable free evaluation tree*** and the corresponding term is called ***variable free term***

Some operations which are performed during the knowledge base processing requires a more specific classification of the variable free evaluation trees:
  - ***static evaluable tree*** contains only constants and thus can be evaluated during ***analysis phase*** or ***solution phase***
  - ***dynamic evaluation*** contains constants and parameters and thus can be evaluated during ***calculation phase***

    
## Range attribute
Every term has one of the following range attributes
- Unspecified
- Greater-Equal-Zero
- Greater-Zero
- Less-Equal-Zero
- Less-Zero
- Zero
- Non-Zero