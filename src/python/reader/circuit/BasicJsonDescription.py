from reader.circuit.model.Document import Document

from reader.circuit.model.Node import Node
from reader.circuit.model.FixedNode import FixedNode
from reader.circuit.model.FreeNode import FreeNode

from reader.circuit.model.Component import Component
from reader.circuit.model.Resistor import Resistor
from reader.circuit.model.VoltageSource import VoltageSource
from reader.circuit.model.CurrentSource import CurrentSource
from reader.circuit.model.Wire import Wire

from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class BasicJsonDescription(object):

    @staticmethod
    def _parseNodes(element, type:ElementTypeEnum) -> Node:
        if "name" in element:
            name = element["name"]
            if type == ElementTypeEnum.FIXED_NODE:
                if "value" in element:
                    value = float(element["value"])
                    return FixedNode(name,value)
            elif type == ElementTypeEnum.FREE_NODE:
                return FreeNode(name)

        return None

    @staticmethod
    def _parseComponents(element, type:ElementTypeEnum) -> Component:
        if "name" in element:
            name = element["name"]

            if ("pin_in" in element) and ("pin_out" in element):
                pin_in = element["pin_in"]
                pin_out = element["pin_out"]

                if type == ElementTypeEnum.RESISTOR:
                    if "value" in element:
                        value = float(element["value"])
                        return Resistor(name,pin_in,pin_out,value)
                elif type == ElementTypeEnum.VOLTAGE_SOURCE:
                    if "value" in element:
                        value = float(element["value"])
                        return VoltageSource(name,pin_in,pin_out,value)
                elif type == ElementTypeEnum.CURRENT_SOURCE:
                    if "value" in element:
                        value = float(element["value"])
                        return CurrentSource(name,pin_in,pin_out,value)
                elif type == ElementTypeEnum.WIRE:
                    return Wire(name, pin_in, pin_out)

        return None

    @staticmethod
    def _parseElement(element):
        if "type" in element:
            element_type_string = str(element["type"])
            element_type = ElementTypeEnum.deriveEnum(element_type_string)
            
            node_types = {
                ElementTypeEnum.FIXED_NODE,
                ElementTypeEnum.FREE_NODE
            }

            component_type = {
                ElementTypeEnum.RESISTOR,
                ElementTypeEnum.VOLTAGE_SOURCE,
                ElementTypeEnum.CURRENT_SOURCE,
                ElementTypeEnum.WIRE
            }

            if element_type in node_types:
                return BasicJsonDescription._parseNodes(element, element_type)
            elif element_type in component_type:
                return BasicJsonDescription._parseComponents(element, element_type)
            else:
                # Ignore unknown components
                return None
        else:
            return None

    @staticmethod
    def read(document):
        nodes = set()
        components = set()

        if "elements" in document:
            elements = document["elements"]
            if isinstance(elements, list):
                for element in elements:
                    element_obj = BasicJsonDescription._parseElement(element)

                    if isinstance(element_obj, Node):
                        nodes.add(element_obj)
                    elif isinstance(element_obj, Component):
                        components.add(element_obj)

        doc = Document(nodes, components)
        return doc