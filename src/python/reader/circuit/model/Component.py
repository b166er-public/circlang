from reader.circuit.model.Element import Element
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class Component(Element):
    
    pin_in_name: str
    pin_out_name: str

    def __init__(self, type:ElementTypeEnum, name:str, pin_in_name:str, pin_out_name:str):
        super().__init__(type, name)
        self.pin_in_name = pin_in_name
        self.pin_out_name = pin_out_name