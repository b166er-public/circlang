# Circuit simulation thoughts

## Elements of a linear circuit

- Every element has a name (normal identifier rules like in C/C++)

### Components

- Have always two terminals (**IN** and **OUT**)

| Type | Symbol | Unit | Parameter|
|------|--------|------|----------|
| Resistor | **R** | **Ohm** | Resistance of component |
| Voltage Source | **U** | **Volt** | Voltage of source |
| Current Source | **I** | **Ampere** | Current of source |
| Wire | **W** | - | - |

### Nodes
- Free nodes **N**
- Grounded nodes **N0**

## Equations

- Every element of circuit introduce a variable into the system of equations. 
- Every component $i$ introduce a component variable $C_i$ which respresents the current going through the component. The value of this variable is positive iff the electrons enter the component at the terminal **IN** and leaves the component at the terminal **OUT**. Otherwise the value of the variable is negative. Zero means that no current is traveling through the component at all.
- Every node $j$ introduce a node variable $N_j$ which represents the voltage at this node.

### Components

- Every component has a terminal **IN** and a terminal **OUT**. As in a circuit every terminal is directly connected to a single node, we will use the node variables instead. In equation we will call the node variable which represents the **IN** terminal with $N_I$ and the node variable which represents the **OUT** terminal $N_O$ accordingly.
- A component is only ***properly connected*** to the circuit if both terminals are connected to different nodes. This nodes have to be ***properly connected*** to the circuit.

#### Resistor
If componet $i$ is a resistor and $R$ is its resistance value in ***Ohm*** we can write down following equation for this component:
$$R=U/I$$
$$R=(N_I - N_O)/C_i$$
$$R \cdot C_i=N_I - N_O $$
$$R \cdot C_i + N_O - N_I = 0 $$

#### Voltage Source
If component $i$ is a volage source and $U$ is its voltage between the terminals in ***Volt*** we can write the following equation:
$$N_O - N_I = U$$

#### Current Source
If component $i$ is a current source and $I$ is its current in ***Ampere*** traveling through the component from the terminal **IN** to the terminal **OUT** we can write the following equation:
$$C_i = I$$

#### Wire
If component $i$ is a wire that means the voltage difference between both terminals is equal to 0. This way we can formulate following equation:
$$N_O = N_I$$
$$N_O - N_I = 0$$
which is also equal to
$$N_I - N_O = 0$$

### Nodes

- Every node which is ***properly connected*** to the circuit has at least two terminals of different components connected to it. Otherwise the node is per definition not ***properly connected*** to the circuit.

#### Free node

- Let be $X$ the set of component index which has their **IN** terminal connected to this node.
- Let be $Y$ the set of component index which had their **OUT** terminal connected to this node

We can formulate following equation for every free node:

$$\sum_{x \in X}C_x - \sum_{y \in Y}C_y = 0$$

Or in other words, the current which flows into a node is equal to the current which leaves a node.

#### Grounded node

Let be $j$ a grounded node so per definition we assume here a voltage level of 0 and thus the equation is simply:
$$N_j = 0$$
