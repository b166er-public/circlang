from reader.circuit.model.Element import Element
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class Node(Element):
    
    def __init__(self, type:ElementTypeEnum, name:str):
        super().__init__(type,name)