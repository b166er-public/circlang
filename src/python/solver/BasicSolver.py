from typing import List, Set, Dict, Optional

from reader.circuit.model.Document import Document

from reader.circuit.model.Node import Node
from reader.circuit.model.FreeNode import FreeNode
from reader.circuit.model.FixedNode import FixedNode

from reader.circuit.model.Component import Component
from reader.circuit.model.CurrentSource import CurrentSource
from reader.circuit.model.VoltageSource import VoltageSource
from reader.circuit.model.Resistor import Resistor
from reader.circuit.model.Wire import Wire

import logging as log
from texttable import Texttable
import numpy as np

class Solution(object):
    document: Document
    
    node_variable_mapping: Dict[str, int]
    component_variable_mapping: Dict[str, int]
    
    node_equation_mapping: Dict[str, int]
    component_equation_mapping: Dict[str, int]

    number_of_nodes: int
    number_of_components: int
    number_of_elements: int

    circuit_matrix: np.ndarray
    circuit_inhomogeneity: np.ndarray
    circuit_solution: np.ndarray

    is_valid: bool

    def __init__(self):
        self.node_variable_mapping = dict()
        self.component_variable_mapping = dict()

        self.node_equation_mapping = dict()
        self.component_equation_mapping = dict()

        self.number_of_nodes = 0
        self.number_of_components = 0
        self.number_of_elements = 0

        self.circuit_matrix = np.zeros(shape=(1,1), dtype=np.double)
        self.circuit_inhomogeneity = np.zeros(shape=(1,1), dtype=np.double)
        self.circuit_solution = np.zeros(shape=(1,1), dtype=np.double)

        self.is_valid = False

    def print_solution(self):
        log.info("")
        log.info("----------------")
        log.info("System solution")
        log.info("----------------")
        log.info("")

        if(self.is_valid):
            for node_name in self.node_variable_mapping.keys():
                node_variable_index = self.node_variable_mapping[node_name]
                node_voltage = self.circuit_solution[node_variable_index]
                log.info(f"Node [ {node_name} ] = {float(node_voltage)} V")

            for component_name in self.component_variable_mapping.keys():
                component_variable_index = self.component_variable_mapping[component_name]
                component_current = self.circuit_solution[component_variable_index]
                log.info(f"Component [ {component_name} ] = {float(component_current)} A")
        else:
            log.warning(f"No solution found as circuit generates singular matrix!")

    def get_variable_list(self) -> List[str]:
        result = [""] * self.number_of_elements
        for node_name in self.node_variable_mapping.keys():
            result[self.node_variable_mapping[node_name]] = "N:" + node_name
        for component_name in self.component_variable_mapping.keys():
            result[self.component_variable_mapping[component_name]] = "C:" + component_name
        return result
    
    def get_equation_list(self) -> List[str]:
        result = [""] * self.number_of_elements
        for node_name in self.node_equation_mapping.keys():
            result[self.node_equation_mapping[node_name]] = "N:" + node_name
        for component_name in self.component_equation_mapping.keys():
            result[self.component_equation_mapping[component_name]] = "C:" + component_name
        return result

    def print_equation_system(self):
        log.info("")
        log.info("----------------")
        log.info("Equation system")
        log.info("----------------")
        log.info("")
        variable_list = self.get_variable_list()
        equation_list = self.get_equation_list()
        
        table:Texttable = Texttable()
        header = ["Equation"] + variable_list + ["Inhomogeneity"]
        table.add_row(header)
        for e in range(self.number_of_elements):
            e_list = []
            e_list.append(equation_list[e])
            for v in range(self.number_of_elements):
                e_list.append(float(self.circuit_matrix[e,v]))
            e_list.append(float(self.circuit_inhomogeneity[e]))
            table.add_row(e_list)
        
        for line in table.draw().splitlines():
            log.info(line)

    def print_elements(self):
        log.info("")
        log.info("----------------")
        log.info("System elements")
        log.info("----------------")
        log.info("")

        table_nodes:Texttable = Texttable()
        header_nodes = ["Type", "Name", "Parameter"]
        table_nodes.add_row(header_nodes)
        for node_name in self.document.get_node_namespace():
            node = self.document.get_node_by_name(node_name)
            if isinstance(node, FixedNode):
                fn:FixedNode = node
                table_nodes.add_row(["FixedNode",node_name,fn.voltage])
            elif isinstance(node, FreeNode):
                n:FreeNode = node
                table_nodes.add_row(["FreeNode",node_name,"-"])

        log.info("")
        log.info("Nodes :")
        log.info("")

        for line in table_nodes.draw().splitlines():
            log.info(line)

        table_components:Texttable = Texttable()
        header_components = ["Type", "Name", "Pin IN", "Pin OUT", "Parameter"]
        table_components.add_row(header_components)
        for component_name in self.document.get_component_namespace():
            component = self.document.get_component_by_name(component_name)
            if isinstance(component, Resistor):
                r:Resistor = component
                table_components.add_row(["Resistor",component_name,r.pin_in_name,r.pin_out_name,r.resistance])
            elif isinstance(component, VoltageSource):
                vs:VoltageSource = component
                table_components.add_row(["VoltageSource",component_name,vs.pin_in_name,vs.pin_out_name,vs.voltage])
            elif isinstance(component, CurrentSource):
                cs:CurrentSource = component
                table_components.add_row(["CurrentSource",component_name,cs.pin_in_name,cs.pin_out_name,cs.current])
            elif isinstance(component, Wire):
                w:Wire = component
                table_components.add_row(["Wire",component_name,w.pin_in_name,w.pin_out_name,"-"])
            
        log.info("")
        log.info("Components :")
        log.info("")

        for line in table_components.draw().splitlines():
            log.info(line)

class BasicSolver(object):
    def __init__(self):
        pass

    def _count_nodes_and_components(self, solution:Solution):
        solution.number_of_nodes = len(solution.document.get_node_namespace())
        solution.number_of_components = len(solution.document.get_component_namespace())
        solution.number_of_elements = solution.number_of_nodes + solution.number_of_components

    def _build_node_variable_mapping(self, solution:Solution):
        nn = solution.document.get_node_namespace()
        counter = int(0)
        for name in nn:
            solution.node_variable_mapping[name] = counter
            counter = int(counter + 1)

    def _build_component_variable_mapping(self, solution:Solution):
        cn = solution.document.get_component_namespace()
        counter = int(solution.number_of_nodes)
        for name in cn:
            solution.component_variable_mapping[name] = counter
            counter = int(counter + 1)

    def _build_node_equation_mapping(self, solution:Solution):
        counter = int(0)
        for k in solution.node_variable_mapping.keys():
            solution.node_equation_mapping[k] = counter
            counter = int(counter + 1)

    def _build_component_equation_mapping(self, solution:Solution):
        counter = solution.number_of_nodes
        for k in solution.component_variable_mapping.keys():
            solution.component_equation_mapping[k] = counter
            counter = int(counter + 1)

    def _initialize_matrix_and_inhomogeneity(self, solution:Solution):
        matrix_dimension:int = solution.number_of_elements
        solution.circuit_matrix = np.zeros(
            shape=(matrix_dimension,matrix_dimension),
            dtype=np.double
        )

        solution.circuit_inhomogeneity = np.zeros(
            shape=(matrix_dimension,1),
            dtype=np.double
        )

    def _build_up_node_equation(self, solution:Solution, node_name:str):
        equation_index = solution.node_equation_mapping[node_name]
        node_object = solution.document.get_node_by_name(node_name)
        
        if(isinstance(node_object, Node)):
            
            if(isinstance(node_object,FreeNode)):
                # Collect all components with pin in or out attached
                pin_in_component_set = set()
                pin_out_component_set = set()
                for component_name in solution.document.get_component_namespace():
                    component:Component = solution.document.get_component_by_name(component_name)
                    if(component.pin_in_name == node_name):
                        pin_in_component_set.add(component_name)
                    elif(component.pin_out_name == node_name):
                        pin_out_component_set.add(component_name)

                # Define equation based on topology        
                for pin_in in pin_in_component_set:
                    component_variable_index:int = solution.component_variable_mapping[pin_in]
                    solution.circuit_matrix[equation_index,component_variable_index] = 1.0
                for pin_out in pin_out_component_set:
                    component_variable_index:int = solution.component_variable_mapping[pin_out]
                    solution.circuit_matrix[equation_index,component_variable_index] = -1.0
                solution.circuit_inhomogeneity[equation_index] = 0.0

            elif(isinstance(node_object,FixedNode)):
                fixed_node:FixedNode = node_object
                node_variable_index:int = solution.node_variable_mapping[node_name]
                solution.circuit_matrix[equation_index,node_variable_index] = 1.0
                solution.circuit_inhomogeneity[equation_index] = fixed_node.voltage
        

    def _build_up_component_equation(self, solution:Solution, component_name:str):
        equation_index = solution.component_equation_mapping[component_name]
        component_object = solution.document.get_component_by_name(component_name)
        
        if(isinstance(component_object, Component)):

            component:Component = component_object
            component_variable_index:int = solution.component_variable_mapping[component_name]
            pin_in_variable_index:int = solution.node_variable_mapping[component.pin_in_name]
            pin_out_variable_index:int = solution.node_variable_mapping[component.pin_out_name]

            if(isinstance(component_object, Resistor)):
                resistor:Resistor = component_object
                solution.circuit_matrix[equation_index,pin_in_variable_index] = -1.0
                solution.circuit_matrix[equation_index,pin_out_variable_index] = 1.0
                solution.circuit_matrix[equation_index,component_variable_index] = resistor.resistance
                solution.circuit_inhomogeneity[equation_index] = 0.0
            elif(isinstance(component_object, VoltageSource)):
                voltage_source:VoltageSource = component_object
                solution.circuit_matrix[equation_index,pin_out_variable_index] = 1.0
                solution.circuit_matrix[equation_index,pin_in_variable_index] = -1.0
                solution.circuit_inhomogeneity[equation_index] = voltage_source.voltage
            elif(isinstance(component_object, CurrentSource)):
                current_source:CurrentSource = component_object
                solution.circuit_matrix[equation_index,component_variable_index] = 1.0
                solution.circuit_inhomogeneity[equation_index] = component_object.current
            elif(isinstance(component_object, Wire)):
                solution.circuit_matrix[equation_index,pin_in_variable_index] = 1.0
                solution.circuit_matrix[equation_index,pin_out_variable_index] = -1.0
                solution.circuit_inhomogeneity[equation_index] = 0.0
                

    def _build_up_matrix_and_inhomogeneity(self, solution:Solution):
        for n in solution.node_equation_mapping.keys():
            self._build_up_node_equation(solution, n)

        for c in solution.component_equation_mapping.keys():
            self._build_up_component_equation(solution, c)

    def _calculate_circuit_state(self, solution:Solution):
        # Check if computation is possible at all
        is_non_singular:bool = (np.linalg.matrix_rank(solution.circuit_matrix) == solution.number_of_elements)

        if(is_non_singular):
            solution.circuit_solution = np.linalg.solve(a=solution.circuit_matrix, b=solution.circuit_inhomogeneity)
            solution.is_valid = True
        else:
            solution.is_valid = False

    def solve(self, document:Document):
        solution = Solution()
        solution.document = document
        self._count_nodes_and_components(solution)
        self._build_node_variable_mapping(solution)
        self._build_component_variable_mapping(solution)
        self._build_node_equation_mapping(solution)
        self._build_component_equation_mapping(solution)
        self._initialize_matrix_and_inhomogeneity(solution)
        self._build_up_matrix_and_inhomogeneity(solution)
        self._calculate_circuit_state(solution)
        return solution
