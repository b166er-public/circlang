import unittest
import json
import os
import sys

from reader.circuit.BasicJsonDescription import BasicJsonDescription
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class TestBasicJsonDescriptionReader(unittest.TestCase):

    def setUp(self):
        test_file_names = [
            "example_circuit_1.json",
            "example_circuit_2.json",
            "example_circuit_3.json"
        ]

        self.test_file_jsons = {}

        for tfn in test_file_names:
            path = os.path.join(os.path.dirname(__file__), tfn)
            with open(path) as json_file:
                self.test_file_jsons[tfn] = json.load(json_file)

    def test_read_example_circuit_1(self):
        # Process JSON file
        document = BasicJsonDescription.read(
            self.test_file_jsons["example_circuit_1.json"]
        )
   
        # Validate content
        self.assertIsNotNone(document.get_node_by_name("x"))
        self.assertIsNotNone(document.get_node_by_name("y"))
        self.assertIsNone(document.get_node_by_name("z"))

    def test_read_example_circuit_2(self):
        # Process JSON file
        document = BasicJsonDescription.read(
            self.test_file_jsons["example_circuit_2.json"]
        )
   
        # Validate content
        self.assertIsNotNone(document.get_node_by_name("x"))
        self.assertTrue(document.get_node_by_name("x").type == ElementTypeEnum.FREE_NODE)

        self.assertIsNotNone(document.get_node_by_name("y"))
        self.assertTrue(document.get_node_by_name("y").type == ElementTypeEnum.FREE_NODE)

        self.assertIsNotNone(document.get_node_by_name("abc"))
        self.assertTrue(document.get_node_by_name("abc").type == ElementTypeEnum.FIXED_NODE)
        self.assertAlmostEqual(document.get_node_by_name("abc").voltage, 30.0)

        self.assertIsNotNone(document.get_component_by_name("r1"))
        self.assertTrue(document.get_component_by_name("r1").type == ElementTypeEnum.RESISTOR)
        self.assertAlmostEqual(document.get_component_by_name("r1").resistance, 0.1)

        self.assertIsNotNone(document.get_component_by_name("r2"))
        self.assertTrue(document.get_component_by_name("r2").type == ElementTypeEnum.RESISTOR)
        self.assertAlmostEqual(document.get_component_by_name("r2").resistance, 1.4)

        self.assertIsNotNone(document.get_component_by_name("v1"))
        self.assertTrue(document.get_component_by_name("v1").type == ElementTypeEnum.VOLTAGE_SOURCE)
        self.assertAlmostEqual(document.get_component_by_name("v1").voltage, 60.0)

        self.assertIsNotNone(document.get_component_by_name("i1"))
        self.assertTrue(document.get_component_by_name("i1").type == ElementTypeEnum.CURRENT_SOURCE)
        self.assertAlmostEqual(document.get_component_by_name("i1").current, 5.5)

if __name__ == '__main__':
    unittest.main()