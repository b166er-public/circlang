from reader.circuit.model.Node import Node
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class FixedNode(Node):
    
    voltage: float

    def __init__(self, name:str, voltage:float):
        super().__init__(ElementTypeEnum.FIXED_NODE, name)
        self.voltage = voltage