import unittest
import json
import os
import sys
import logging as log

from reader.circuit.model.Document import Document
from solver.BasicSolver import BasicSolver, Solution

class TestBasicSolver(unittest.TestCase):

    def setUp(self):
        pass


    def solve_circuit(self, circuit:Document, name:str):
        log.basicConfig(
            force=True,
            level=log.DEBUG, 
            filename=f"logfile_{name}.log", 
            filemode="w",
            format="%(asctime)-15s %(levelname)-8s %(message)s" )
   
        # Find solution
        solver = BasicSolver()
        solution = solver.solve(circuit)

        # Print solution
        solution.print_elements()
        solution.print_equation_system()
        solution.print_solution()

        return solution

    def test_solve_circuit_1(self):

        circuit = Document()
        circuit.add_fixed_node("G",0.0)
        circuit.add_free_node("N1")
        circuit.add_free_node("N2")
        circuit.add_voltage_source("V1","G","N1", 5.0)
        circuit.add_resistor("R1", "N1", "N2", 1.0)
        circuit.add_resistor("R2", "N2", "G", 1.0)

        solution:Solution = self.solve_circuit(circuit,"1")
        
        self.assertTrue(solution.is_valid)

    def test_solve_circuit_2(self):

        circuit = Document()
        circuit.add_fixed_node("G",0.0)
        circuit.add_fixed_node("P5",5.0)
        circuit.add_wire("W1", "P5", "G")
   
        solution:Solution = self.solve_circuit(circuit, "2")

        self.assertFalse(solution.is_valid)

    def test_solve_circuit_3(self):

        circuit = Document()
        circuit.add_fixed_node("G",0.0)
        circuit.add_free_node("N1")
        circuit.add_fixed_node("P5",5.0)

        circuit.add_wire("W1", "N1", "G")
        circuit.add_resistor("R1","P5","N1", 2.5)
   
        solution:Solution = self.solve_circuit(circuit, "3")

        self.assertTrue(solution.is_valid)

    

if __name__ == '__main__':
    unittest.main()