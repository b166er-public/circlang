from reader.circuit.model.Node import Node
from reader.circuit.model.ElementTypeEnum import ElementTypeEnum

class FreeNode(Node):
    def __init__(self, name:str):
        super().__init__(ElementTypeEnum.FREE_NODE, name)