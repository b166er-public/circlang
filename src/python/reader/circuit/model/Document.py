from typing import List, Set, Optional

from reader.circuit.model.Node import Node
from reader.circuit.model.FreeNode import FreeNode
from reader.circuit.model.FixedNode import FixedNode

from reader.circuit.model.Component import Component
from reader.circuit.model.CurrentSource import CurrentSource
from reader.circuit.model.VoltageSource import VoltageSource
from reader.circuit.model.Resistor import Resistor
from reader.circuit.model.Wire import Wire

class Document(object):
    nodes: Set[Node]
    components: Set[Component]

    def __init__(self, nodes=None, components=None):
        if nodes is None:
            self.nodes = set()
        else:
            self.nodes = nodes

        if components is None:
            self.components = set()
        else:            
            self.components = components

    def get_node_by_name(self, name:str) -> Optional[Node]:
        for node in self.nodes:
            if node.name == name:
                return node
            
        return None
    
    def get_component_by_name(self, name:str) -> Optional[Component]:
        for component in self.components:
            if component.name == name:
                return component
            
        return None
    
    def get_node_namespace(self) -> Set[str]:
        keyset = set()
        for node in self.nodes:
            keyset.add(node.name)
        
        return keyset

    def get_component_namespace(self) -> Set[str]:
        keyset = set()
        for component in self.components:
            keyset.add(component.name)
        
        return keyset
    
    def does_component_exist(self, name:str) -> bool:
        for component in self.components:
            if component.name == name:
                return True
            
        return False
    
    def does_node_exist(self, name:str) -> bool:
        for node in self.nodes:
            if node.name == name:
                return True
            
        return False

    def add_free_node(self, name:str) -> Optional[FreeNode]:
        if not self.does_node_exist(name):
            n = FreeNode(name)
            self.nodes.add(n)
            return n
        else:
            return None
        
    def add_fixed_node(self, name:str, voltage:float) -> Optional[FixedNode]:
        if not self.does_node_exist(name):
            n = FixedNode(name,voltage)
            self.nodes.add(n)
            return n
        else:
            return None

    def _check_if_component_can_be_created(self, name:str, pin_in_name:str, pin_out_name:str) -> bool:
        component_exists = self.does_component_exist(name)
        pin_in_exist = self.does_node_exist(pin_in_name)
        pin_out_exist = self.does_node_exist(pin_out_name)
        return (not component_exists) and pin_in_exist and pin_out_exist

    def add_current_source(self, name:str, pin_in_name:str, pin_out_name:str, current:float) -> Optional[CurrentSource]:
        if self._check_if_component_can_be_created(name, pin_in_name, pin_out_name):
            c = CurrentSource(name, pin_in_name, pin_out_name, current)
            self.components.add(c)
            return c
        else:
            return None
        
    def add_voltage_source(self, name:str, pin_in_name:str, pin_out_name:str, voltage:float) -> Optional[VoltageSource]:
        if self._check_if_component_can_be_created(name, pin_in_name, pin_out_name):
            c = VoltageSource(name, pin_in_name, pin_out_name, voltage)
            self.components.add(c)
            return c
        else:
            return None
        
    def add_resistor(self, name:str, pin_in_name:str, pin_out_name:str, resistance:float) -> Optional[Resistor]:
        if self._check_if_component_can_be_created(name, pin_in_name, pin_out_name):
            c = Resistor(name, pin_in_name, pin_out_name, resistance)
            self.components.add(c)
            return c
        else:
            return None
        
    def add_wire(self, name:str, pin_in_name:str, pin_out_name:str) -> Optional[Wire]:
        if self._check_if_component_can_be_created(name, pin_in_name, pin_out_name):
            c = Wire(name, pin_in_name, pin_out_name)
            self.components.add(c)
            return c
        else:
            return None