from enum import Enum

class ElementTypeEnum(Enum):
    RESISTOR = 1
    VOLTAGE_SOURCE = 2
    CURRENT_SOURCE = 3
    WIRE = 4
    FIXED_NODE = 5
    FREE_NODE = 6
    UNKNOWN = 7

    @staticmethod
    def deriveEnum(literal:str):
        literal_lowercase = literal.lower()
        if literal_lowercase.startswith("r"):
            return ElementTypeEnum.RESISTOR
        elif literal_lowercase.startswith("v"):
            return ElementTypeEnum.VOLTAGE_SOURCE
        elif literal_lowercase.startswith("c") or literal_lowercase.startswith("i"):
            return ElementTypeEnum.CURRENT_SOURCE
        elif literal_lowercase.startswith("w"):
            return ElementTypeEnum.WIRE
        elif literal_lowercase.startswith("n"):
            return ElementTypeEnum.FREE_NODE
        elif literal_lowercase.startswith("f"):
            return ElementTypeEnum.FIXED_NODE
        else:
            return ElementTypeEnum.UNKNOWN